---
title: "Resting State Data Analysis - ANR Reorg"
author: "C�dric Pichat"
date: "`r Sys.Date()`"
output: 
  html_document:
    toc: true
    toc_depth: 4
    toc_float: true
    number_sections: true
    theme: flatly
    highlight: default
---

```{r setup, include=FALSE, echo=FALSE}
knitr::opts_chunk$set(echo = TRUE)
options(java.parameters = "-Xmx4g" ) 

library(xlsx)
library(knitr)
library(kableExtra)
library(ggplot2)
library(RColorBrewer)
library(corrplot)
library(PMCMRplus)
library(psycho)
library(ggcorrplot)
library(brainGraph)
library(abind)
library(data.table)
library(rcartocolor)
library(circlize)
library(tidyverse)
library(viridis)

```


```{r Variable declaration, echo=FALSE}

#########################################################
# TO IMPLEMENT BY THE USER
#########################################################

rm(list=ls())

# Document's directory
path_doc =choose.dir(default = "", caption = "Select Scripts's folder")
opts_knit$set(root.dir = path_doc)

# Path directory / files
path_subject = choose.files(default = path_doc, caption = "Select subject list file")
path_ROIs = choose.files(default = path_doc, caption = "Select ROIs list file")
path = choose.files(default = path_doc, caption = "Select Correlation matrix Conn file")
path_param = choose.files(default = path_doc, caption = "Select parameters file")

path_Graph_Theory = choose.dir(default = path_doc, caption = "Select Graph Theory's folder")

source(path_param)

#########################################################
# END OF IMPLEMENTATION
#########################################################
```


```{r Global Variable, echo=FALSE,message=FALSE, warning=FALSE}
#########################################################
# DECLARATION OF GLOBAL VARIABLES
#########################################################
nHealthy = length(Healthy)
nTLE = length(TLE)
nLTLE = length(LTLE)
nRTLE = length(RTLE)
nSubj = nHealthy + nLTLE + nRTLE
gp=c(rep("Healthy",nHealthy),rep("RTLE",nRTLE),rep("LTLE",nLTLE)) # Reorganised group
gp_conn = c(1:nSubj)
gp_conn[Healthy]="Healthy"
gp_conn[RTLE]="RTLE"
gp_conn[LTLE]="LTLE"
Data_first_subj = read.xlsx2(path,sheetIndex=1,startRow=2,header=FALSE)
nROI= dim(Data_first_subj)[1]  # Number of ROIs
nElem = nROI*(nROI-1)/2 # Number of connexion
Title_raw = read.xlsx2(path_ROIs,sheetIndex=1,header=TRUE)
Title = as.character(Title_raw$AICHA.ROIs)
```

# Material and method

## Resting state protocol and MRI acquisition

400 dynamics 

TR : 2 s

36 axial slices

Resolution : 3x3x3.5 mm

Duration : 13mn20


## Rs-fMRI preprocessing

Preprocessing : SPM12 (Slice Timing, Realignement, Coregistration, Normalisation, 8mm Smooth)

Denoising : Conn toolbox (https://www.nitrc.org/projects/conn)

## Node definition

LMN Network (specific patients hippocampi masks with VolBrain)

# Functional connectivity

## ROIs size influence

```{r ROis size influence, message=FALSE}
source(paste(path_doc,'SizeROI_Corr_2.R',sep="/"))
```


## Movement influence

    
```{r With the mean of the euclidian norm, message=FALSE}
source(paste(path_doc,'Mvt_Corr.R',sep="/"))
```
  

## Correlation value histograms 

```{r Correlation values histograms, message=FALSE}
source(paste(path_doc,'Histogram_Healthy.R',sep="/"))
source(paste(path_doc,'Histogram_LTLE.R',sep="/"))
source(paste(path_doc,'Histogram_RTLE.R',sep="/"))

```

## Correlation matrix

```{r Correlation matrices,dpi = 300, message=FALSE}
source(paste(path_doc,'SD_Mean_Healthy.R',sep="/"))
source(paste(path_doc,'SD_Mean_LTLE.R',sep="/"))
source(paste(path_doc,'SD_Mean_RTLE.R',sep="/"))
```

## Healthy Connectogram of correlation values

```{r Connectogram,dpi = 300, message=FALSE}
source(paste(path_doc,'Connectogram_4_1_Healthy_2_Negative.R',sep="/"))
source(paste(path_doc,'Connectogram_4_1_Healthy_2_Positive.R',sep="/"))
```


## Group statistical analysis - T Test, Welch Two Sample t-test

Multiple comparison method : **`r adj_method`**

Multiple comparson threshold : **`r p_th_adj`**.

**Matrices representation:**

```{r T test, echo=FALSE, message=FALSE,dpi = 300}
source(paste(path_doc,'T_Test_ROI-to-ROI_adjust.R',sep="/"))
```

**Connectogram representation:**

```{r T test connectogram, echo=FALSE, message=FALSE,dpi = 300}
source(paste(path_doc,'Connectogram_4_1_Pat_vs_Healthy_2.R',sep="/"))
```

# Graph theory Analysis


## Matrix adjacency threshold

**Sparsity = 0.05**

```{r Matrix adjacency threshold 0.05, echo=FALSE, message=FALSE, warning=FALSE}
fich = paste(path_Graph_Theory,"0pt05.csv",sep="/")
source(paste(path_doc,'Eglob-Eloc_Corr_1.R',sep="/"))
```
  
**Sparsity = 0.1**

```{r Matrix adjacency threshold 0.1, echo=FALSE, message=FALSE, warning=FALSE}
fich   = paste(path_Graph_Theory,"0pt1.csv",sep="/")
source(paste(path_doc,'Eglob-Eloc_Corr_1.R',sep="/"))
```

**Sparsity = 0.2**

```{r Matrix adjacency threshold 0.2, echo=FALSE, message=FALSE, warning=FALSE}
fich   = paste(path_Graph_Theory,"0pt2.csv",sep="/")
source(paste(path_doc,'Eglob-Eloc_Corr_1.R',sep="/"))
```

**Sparsity = 0.3**

```{r Matrix adjacency threshold 0.3, echo=FALSE, message=FALSE, warning=FALSE}
fich   = paste(path_Graph_Theory,"0pt3.csv",sep="/")
source(paste(path_doc,'Eglob-Eloc_Corr_1.R',sep="/"))
```

**Sparsity = 0.4**

```{r Matrix adjacency threshold 0.4, echo=FALSE, message=FALSE, warning=FALSE}
fich   = paste(path_Graph_Theory,"0pt4.csv",sep="/")
source(paste(path_doc,'Eglob-Eloc_Corr_1.R',sep="/"))
```

## Group Analysis

### Hub Disruption Index (HDI)

**HDI, sparsity = 0.05**

```{r HDI 0.05, echo=FALSE, message=FALSE, warning=FALSE}
fich   = paste(path_Graph_Theory,"0pt05.csv",sep="/")
source(paste(path_doc,'Hub_disrupt_index_2.R',sep="/"))
```

### Network metric analysis - T Test, Welch Two Sample t-test

Threshold : **`r p_th_Network`**

**Global efficiency, sparsity = 0.05**

```{r Network T test Global efficiency 0.05, echo=FALSE, message=FALSE, warning=FALSE}
fich   = paste(path_Graph_Theory,"0pt05.csv",sep="/")
source(paste(path_doc,'Global_Efficiency_Network_T_Test.R',sep="/"))
```
  
**Local efficiency, sparsity = 0.05**

```{r Network T test Local efficiency 0.05, echo=FALSE, message=FALSE, warning=FALSE}
fich   = paste(path_Graph_Theory,"0pt05.csv",sep="/")
source(paste(path_doc,'Local_Efficiency_Network_T_Test.R',sep="/"))
```


### Nodal metric analysis - T Test, Welch Two Sample t-test

Multiple comparison method : **`r adj_method`** 

Multiple comparson threshold : **`r p_th_adj`**

**Nodal Efficiency, sparsity = 0.05**

```{r Nodal T test Nodal efficiency 0.05, echo=FALSE, message=FALSE, warning=FALSE}
fich   = paste(path_Graph_Theory,"0pt05.csv",sep="/")
source(paste(path_doc,'Nodal_Efficiency_T_Test_adjust.R',sep="/"))
```

**Local Efficiency, sparsity = 0.05**

```{r Nodal T test Local efficiency 0.05, echo=FALSE, message=FALSE, warning=FALSE}
fich   = paste(path_Graph_Theory,"0pt05.csv",sep="/")
source(paste(path_doc,'Local_Efficiency_T_Test_adjust.R',sep="/"))
```
