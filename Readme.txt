
Here are instructions for getting the R markdown file that contains results and figures from the submitted article (need R environment; and R markdown packages to edit document).

1) Run "Packages_installation.R" to install needed packages.
1) Run "Analyse_Reorg.Rmd" with R markdown button "Knit".
2) Choose "Scripts" folder at the first windows to locate needed scripts. 
3) Choose "Subjects_List.xlsx" file in "Documents" folder (description of subject's group).
4) Choose "ROIs_List.xlsx" file in "Documents" folder (description of ROIs network).
5) Choose "Matrices_correlations_Conn.xls" file in "Documents" folder (Subject's correlation matrices; one  subject per tab; export from "Conn").
6) Choose "Parameters.R" file in "Documents" folder (specific parameters).
7) Choose "Results_Graph_Theory_Conn" folder in "Documents" folder (graph theory files; export from Conn).